This is a collection of Matlab scripts and Mathematica notebooks I accumulated while doing research with Munday Lab
@ UMD College Park. Most of the Code centers around the simulation of nematic liquid crystal hemispheres. I won't go
too far into that in this README, but if you somehow stumble accross this and are curious shoot me an email
(sgillen@umd.edu).

Fair warning this code is a MESS, possibly the messiest code base I've ever created, it's a lot of scratch work or
something closely resembling it. I'm not going to clean it up though unless someone else needs to use it.

Shout out to David Somers, who wrote a fair amount of this code, and guided me through all the complicated math
throughout. 

